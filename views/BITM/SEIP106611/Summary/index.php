<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProjects'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
    
use \App\BITM\SEIP106611\Summary\Summary;
use \App\BITM\SEIP106611\Utility\Utility;

    $contant = new Summary();
    $contants = $contant->index();
    
    
    
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Summary of Organization</title>
	<link rel="stylesheet" href="../../../../Resource/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../../Resource/css/style.css">
	
	<style type="text/css">
		.in {
		display: inline;
	}
		.text_align{
		text-align:center;
	}
	</style>
	
  </head>
  <body>
		<div class="wrapper">
			<div class="container bg">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<h1>List of User Summary</h1>
						<br />
						<br />
						<br />

						<div class="message"><?= Utility::message(); ?></div>
						<a class="btn btn-success btn-xs" href="create.php">Add New</a>
						<br />
						<br />
						
						
							<div class="listArea">
								<div class="tableInfo">
								
									<div class="row">
									
										<div class=" col-xs-4 col-sm-3 col-md-2">
											<form class="ajax" action="#" method="post">
												<select class="items" name="items">
													<option value="15">10</option>
													<option value="20">20</option>
													<option value="30">30</option>
													<option value="40">40</option>
												</select>
											</form>
										</div>
										
										<div class=" col-md-2">
											<a  class="btn btn-success btn-xs" href="#">SEARCH</a>
										</div>
										
										<div class="col-xs-8 col-sm-9 col-md-8">
																							
											<div class="row">
												<div class="col-xs-7 col-sm-8 col-md-4">
																
													<p class="textRight">Download list as:</p>
												</div>
												<div class="col-xs-5 col-sm-4 col-md-4">
																	
													<a class="btn btn-success btn-xs" href="#">PDF</a>
																		
													<a class="btn btn-success btn-xs" href="#">EXCEL</a>
													<a class="btn btn-warning btn-xs" href="trashed.php" class="list-btn">Trashed List</a>
																		
												</div>
											</div>
										</div>
									</div>
									
								</div>

									<table class="table table-bordered">
										<thead class="text-center">
											<tr>
												<th  class="text_align">Serial</th>
												<th  class="text_align">Name &dArr;</th>
												<th  class="text_align">Summary &dArr;</th>
												<th  class="text_align">Action</th>
											</tr>
										</thead>
										<tbody class="text-center">
											<?php
											   $count =1;
											   foreach($contants as $contant){
											?>
											<tr>
												<td><?php echo $count;?></td>
												<td><a href="show.php?id=<?php echo $contant->id;?>"><?php echo $contant->name;?></a></td>
												<td><?php echo $contant->contant;?></td>
												<td>
													<a class="btn btn-primary btn-xs" href="show.php?id=<?php echo $contant->id;?>" class="list-btn">View</a>
													<a class="btn btn-info btn-xs" href="edit.php?id=<?php echo $contant->id;?>" class="list-btn">Edit</a> 
														  <form class="in" action="delete.php" method="post">
															 <input type="hidden" name ="id" value="<?php echo $contant->id;?>">
															  <button class="btn btn-danger btn-xs" type="submit" class="delete">Delete</button>
														  </form>
													<a class="btn btn-warning btn-xs" href="trash.php?id=<?php echo $contant->id;?>" class="list-btn">Trash</a>
													<a class="btn btn-success btn-xs" href="" class="list-btn">Share With Friend</a>
												</td>
											</tr>
											<?php
												$count++;
											}
											?>
										
										</tbody>
									</table>
							</div>
					</div>
				</div>	
			</div>
		</div>
      <p class="text-center"><a href="../../../../index.php">Go to Homepage</a></p>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
   <script src="../../../../Resource/bootstrap/js/bootstrap.min.js"></script>
    <script>
           $('.delete').bind('click',function(e){
               var deleteItem = confirm("Are you sure you want to delete?");
               if(!deleteItem){
                  //return false; 
                  e.preventDefault();
               }
           }); 
    </script>
  </body>
</html>